#include "header.h"

int main(){
    int userOption;

    printf ("Select an option: \n");
    printf ("1. Print Hello World\n");
    printf ("2. Print all numbers from 1-50\n");

    scanf ("%d", &userOption);

    switch (userOption)
    {
    case 1:
        printHelloWorld();
        break;

    case 2:
        countToFifty();
        break;
    
    default:
        break;
    }
}